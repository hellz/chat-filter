/*

chat_filter.sp

Made by Jomn
Version: 3.0.1

*/

#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <basecomm>
#include <regex>
#include <cstrike>
#include <scp>

#undef REQUIRE_PLUGIN
#include <sourcebans>
#include <timedgags>
#include <sourcecomms>

#define CF_VERSION "3.0.1"
#define MAXCHAR MAXLENGTH_MESSAGE
#define MAXLENGTH_REPLACER 32

#define TYPE_NONE     0
#define TYPE_INSULT   (1<<0)
#define TYPE_ADVERT   (1<<1)

enum FilterType {
    FilterNone = 0,
    FilterCensor,
    FilterHide,
    FilterRename,
    FilterSilence,
    FilterKick,
    FilterBan
}


new bool:g_bSBAvailable = false;
new bool:g_bTGAvailable = false;
new bool:g_bSCAvailable = false;
new bool:g_bSCPAvailable = false;

new Handle:g_aGag = INVALID_HANDLE;
new Handle:g_aGagTime = INVALID_HANDLE;
new Handle:g_aBan = INVALID_HANDLE;
new Handle:g_aBanTime = INVALID_HANDLE;
new Handle:g_aWhitelist = INVALID_HANDLE;
new Handle:g_aCensor = INVALID_HANDLE;
new Handle:g_aCensorReplacers = INVALID_HANDLE;
new Handle:g_aHide = INVALID_HANDLE;
new Handle:g_aNameFilter = INVALID_HANDLE;
new Handle:g_aNameFilterType = INVALID_HANDLE;
new Handle:g_aClanFilter = INVALID_HANDLE;
new Handle:g_aClanFilterType = INVALID_HANDLE;

new bool:g_bPlayerKicked[MAXPLAYERS+1] = {false, ...};

new Handle:g_Cvar_ProtectAdmins = INVALID_HANDLE;
new Handle:g_Cvar_AddTime = INVALID_HANDLE;
new Handle:g_Cvar_CensorReplacers = INVALID_HANDLE;
new Handle:g_Cvar_HideMessage = INVALID_HANDLE;
new Handle:g_Cvar_TagName = INVALID_HANDLE;
new Handle:g_Cvar_RenameName = INVALID_HANDLE;
new Handle:g_Cvar_NameKickReason = INVALID_HANDLE;
new Handle:g_Cvar_ClanKickReason = INVALID_HANDLE;
new Handle:g_Cvar_NameBanReason = INVALID_HANDLE;
new Handle:g_Cvar_ClanBanReason = INVALID_HANDLE;
new Handle:g_Cvar_NameBanTime = INVALID_HANDLE;
new Handle:g_Cvar_ClanBanTime = INVALID_HANDLE;


new bool:g_bProtectAdmins = false;
new bool:g_bAddTime = false;
new String:g_HideMessage[MAXCHAR];
new String:g_TagName[16];
new String:g_NameKickReason[MAXCHAR];
new String:g_ClanKickReason[MAXCHAR];
new String:g_NameBanReason[MAXCHAR];
new String:g_ClanBanReason[MAXCHAR];
new g_NameBanTime;
new g_ClanBanTime;

new String:log[256];

new Handle:hDatabase = INVALID_HANDLE;
new bool:g_bConnected = false;
new bool:g_bLoadedSettings = false;

public Plugin:myinfo = {
    name = "Chat Filter",
    author = "Jomn",
    description = "Filters the chat and takes care of the offenders",
    version = CF_VERSION,
    url = "http://www.hellz.fr/"
};

public OnAllPluginsLoaded() {
    if (LibraryExists("sourcebans"))
        g_bSBAvailable = true;
    if (LibraryExists("timedgags"))
        g_bTGAvailable = true;
    if (LibraryExists("sourcecomms"))
        g_bSCAvailable = true;
    if (LibraryExists("scp"))
        g_bSCPAvailable = true;
}

public OnLibraryAdded(const String:name[]) {
    if (StrEqual(name, "sourcebans"))
        g_bSBAvailable = true;
    if (StrEqual(name, "timedgags"))
        g_bTGAvailable = true;
    if (StrEqual(name, "sourcecomms"))
        g_bSCAvailable = true;
    if (StrEqual(name, "scp"))
        g_bSCPAvailable = true;
}

public OnLibraryRemoved(const String:name[]) {
    if (StrEqual(name, "sourcebans"))
        g_bSBAvailable = false;
    if (StrEqual(name, "timedgags"))
        g_bTGAvailable = false;
    if (StrEqual(name, "sourcecomms"))
        g_bSCAvailable = false;
    if (StrEqual(name, "scp"))
        g_bSCPAvailable = false;
}

public OnPluginStart() {
    new arraySize = ByteCountToCells(MAXLENGTH_MESSAGE);
    g_aGag = CreateArray(arraySize);
    g_aGagTime = CreateArray();
    g_aBan = CreateArray(arraySize);
    g_aBanTime = CreateArray();
    g_aWhitelist = CreateArray(arraySize);
    g_aCensor = CreateArray(arraySize);
    g_aCensorReplacers = CreateArray(ByteCountToCells(MAXLENGTH_REPLACER));
    g_aHide = CreateArray(arraySize);
    g_aNameFilter = CreateArray(arraySize);
    g_aNameFilterType = CreateArray();
    g_aClanFilter = CreateArray(arraySize);
    g_aClanFilterType = CreateArray();
    
    CreateConVar("chatfilter_version", CF_VERSION, _,
        FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY|FCVAR_DONTRECORD);
    g_Cvar_ProtectAdmins = CreateConVar("sm_chatfilter_protectadmins", "0",
        "Enables or disables the protection of admins on the server",
        FCVAR_PLUGIN|FCVAR_SPONLY);
    g_Cvar_AddTime = CreateConVar("sm_chatfilter_addtime", "0",
        "Enables or disables the stacking of gag/ban times",
        FCVAR_PLUGIN);
    g_Cvar_CensorReplacers = CreateConVar("sm_chatfilter_censorreplacers",
        "saperlipopette;sacrebleu;cornegidouille;ventre-saint-gris;crévindiou;boudiou;tomate;pantoufle;%?ù$%!;****;***;*****;&/%?,µ%;****;****;ù%:/,?*%",
        "Words that will randomly replace censored words. The replacers must be separated with ';'. The list can have at most 32 words.",
        FCVAR_PLUGIN);
    g_Cvar_HideMessage = CreateConVar("sm_chatfilter_hidemessage",
        "Surveillez votre langage ! / Watch your language !",
        "Message that will be shown to a player who says a word that is in the Hide list",
        FCVAR_PLUGIN);
    g_Cvar_TagName = CreateConVar("sm_chatfilter_tagname", "",
        "This is the clan tag that the player will get when his current clantag is in the hide list",
        FCVAR_PLUGIN);
    g_Cvar_RenameName = CreateConVar("sm_chatfilter_renamename", "Unamed",
        "This is the name that the player will get when his current name is in the rename list",
        FCVAR_PLUGIN);
    g_Cvar_NameKickReason = CreateConVar("sm_chatfilter_namekickreason",
        "Changez votre pseudo ! / Change your nickname !",
        "Reason that the plugin should apply when he kicks a player because of his name",
        FCVAR_PLUGIN);
    g_Cvar_ClanKickReason = CreateConVar("sm_chatfilter_clankickreason",
        "Changez votre clantag ! / Change your clantag !",
        "Reason that the plugin should apply when he kicks a player because of his clantag",
        FCVAR_PLUGIN);
    g_Cvar_NameBanReason = CreateConVar("sm_chatfilter_namebanreason",
        "Pseudo Incorrect",
        "Reason that the plugin should apply when he bans a player because of his name",
        FCVAR_PLUGIN);
    g_Cvar_ClanBanReason = CreateConVar("sm_chatfilter_clanbanreason",
        "ClanTag Incorrect", "Reason that the plugin should apply when he bans a player because of his clantag",
        FCVAR_PLUGIN);
    g_Cvar_NameBanTime = CreateConVar("sm_chatfilter_namebantime", "1440",
        "Ban length the plugin should apply when he bans a player because of his name",
        FCVAR_PLUGIN);
    g_Cvar_ClanBanTime = CreateConVar("sm_chatfilter_clanbantime", "10080",
        "Ban length the plugin should apply when he bans a player because of his clantag",
        FCVAR_PLUGIN);
    
    
    RegAdminCmd("chatfilter_reload", Command_Reload, ADMFLAG_RCON,
        "Reloads the plugins configs");
    
    BuildPath(Path_SM, log, sizeof(log), "logs/chat_filter.log");
    

    if (!SQL_CheckConfig("chat_filter")) {
        LogToFile(log,
            "Database failure: Could not find Database conf \"chat_filter\".");
        SetFailState(
            "Database failure: Could not find Database conf \"chat_filter\"");
        return;
    }
    
    SQL_TConnect(GotDatabase, "chat_filter");
    
    AutoExecConfig(true, "chat_filter");
}

public OnConfigsExecuted() {
    if (g_bConnected) {
        ResetSettings();
    } else {
        CreateTimer(5.0, Timer_Reset, _, TIMER_FLAG_NO_MAPCHANGE);
    }
}

public Action:Timer_Reset(Handle:timer) {
    if (g_bConnected) {
        ResetSettings();
    } else {
        CreateTimer(5.0, Timer_Reset, _, TIMER_FLAG_NO_MAPCHANGE);
    }
}

public OnClientSettingsChanged(client) {
    decl String:name[MAX_NAME_LENGTH];
    decl String:tag[16];
    decl String:match[MAX_NAME_LENGTH];
    decl String:reason[MAXCHAR];
    new FilterType:type = FilterNone;
    
    if (!g_bConnected)
        return;
    
    if (g_bPlayerKicked[client])
        return;
    
    if (!IsClientInGame(client))
        return;
    
    if (ClientProtected(client) && g_bProtectAdmins)
        return;
    
    if (GetClientName(client, name, sizeof(name))) {
        ConvertToAscii(name);
        type = GetFilterType(g_aNameFilter, g_aNameFilterType, name, match, sizeof(match));
        switch (type){
            case FilterKick: {
                Format(reason, sizeof(reason), "%s (Match Found: %s)", g_NameKickReason, match);
                KickClient(client, reason);
                g_bPlayerKicked[client] = true;
                return;
            }
            case FilterBan: {
                Format(reason, sizeof(reason), "%s (Match Found: %s)", g_NameBanReason, match);
                BanPlayer(client, g_NameBanTime, reason);
                g_bPlayerKicked[client] = true;
                return;
            }
            case FilterRename: {
                decl String:newName[MAX_NAME_LENGTH];
                GetConVarString(g_Cvar_RenameName, newName, sizeof(newName));
                CS_SetClientName(client, newName, true);
                return;
            }
        }
    }
    
    if (CS_GetClientClanTag(client, tag, sizeof(tag))) {
        type = GetFilterType(g_aClanFilter, g_aClanFilterType, tag, match, sizeof(match));
        switch (type) {
            case FilterHide: {
                CS_SetClientClanTag(client, g_TagName);
                return;
            }
            case FilterKick: {
                Format(reason, sizeof(reason), "%s (Match Found: %s)", g_ClanKickReason, match);
                KickClient(client, reason);
                g_bPlayerKicked[client] = true;
                return;
            }
            case FilterBan: {
                Format(reason, sizeof(reason), "%s (Match Found: %s)", g_ClanBanReason, match);
                BanPlayer(client, g_ClanBanTime, reason);
                g_bPlayerKicked[client] = true;
                return;
            }
        }
    }
}

public OnClientDisconnect(client) {
    g_bPlayerKicked[client] = false;
}

public Action OnChatMessage(int &author, Handle recipients, char[] name, char[] message) {
    new minutes = -1;
    new userid;
    ArrayList whitelisted;

    if (author == 0) {
        return Plugin_Continue;
    }

    userid = GetClientUserId(author);

    if (!g_bConnected)
        return Plugin_Continue;
    
    if (ClientProtected(author) && g_bProtectAdmins)
        return Plugin_Continue;

    ProtectString(message, MAXLENGTH_MESSAGE);
    
    whitelisted = CreateArray(ByteCountToCells(MAXLENGTH_MESSAGE));
    HideWhitelistedExpressions(message, MAXLENGTH_MESSAGE, whitelisted);

    minutes = CheckBan(message);
    if (minutes != -1) {
        ShowWhitelistedExpressions(message, MAXLENGTH_MESSAGE, whitelisted);
        CloseHandle(whitelisted);
        UnprotectString(message, MAXLENGTH_MESSAGE);
        DataPack pack;
        CreateDataTimer(0.001, BanPlayerDelayed, pack, TIMER_FLAG_NO_MAPCHANGE);
        pack.WriteCell(userid);
        pack.WriteCell(minutes);
        pack.WriteString(message);
        return Plugin_Stop;
    }

    minutes = CheckSilence(message);
    if (minutes != -1) {
        ShowWhitelistedExpressions(message, MAXLENGTH_MESSAGE, whitelisted);
        CloseHandle(whitelisted);
        UnprotectString(message, MAXLENGTH_MESSAGE);
        DataPack pack;
        CreateDataTimer(0.001, GagPlayerDelayed, pack, TIMER_FLAG_NO_MAPCHANGE);
        pack.WriteCell(userid);
        pack.WriteCell(minutes);
        pack.WriteString(message);
        return Plugin_Stop;
    }

    if (IsHideable(message)) {
        CreateTimer(0.001, SendFilterMessage, userid, TIMER_FLAG_NO_MAPCHANGE);
        ShowWhitelistedExpressions(message, MAXLENGTH_MESSAGE, whitelisted);
        CloseHandle(whitelisted);
        UnprotectString(message, MAXLENGTH_MESSAGE);
        return Plugin_Stop;
    }


    if (CensorMessage(message, MAXLENGTH_MESSAGE)) {
        ShowWhitelistedExpressions(message, MAXLENGTH_MESSAGE, whitelisted);
        CloseHandle(whitelisted);
        UnprotectString(message, MAXLENGTH_MESSAGE);
        return Plugin_Handled;
    }

    ShowWhitelistedExpressions(message, MAXLENGTH_MESSAGE, whitelisted);
    CloseHandle(whitelisted);
    UnprotectString(message, MAXLENGTH_MESSAGE);
    return Plugin_Continue;
}

stock bool CensorMessage(char[] str, int length) {
    int i, substr_nb, size, nb_spaces;
    Handle regex;
    char expression[MAXLENGTH_MESSAGE];
    char subString[MAXLENGTH_MESSAGE];
    char replacer[MAXLENGTH_MESSAGE];
    bool replaced = false;

    size = GetArraySize(g_aCensor);
    for (i = 0; i < size; i++) {
        GetArrayString(g_aCensor, i, expression, sizeof(expression));
        regex = CompileRegex(expression, PCRE_CASELESS|PCRE_UTF8);
        do {
            substr_nb = MatchRegex(regex, str);
            if (substr_nb > 0) {
                GetRegexSubString(regex, 0, subString, sizeof(subString));
                nb_spaces = 0;
                if (subString[0] == ' ') {
                    nb_spaces++;
                }
                GetRandomReplacer(replacer, sizeof(replacer));
                ReplaceString(str, length, subString[nb_spaces], replacer);
                replaced = true;
            }
        } while (substr_nb > 0);
        CloseHandle(regex);
    }

    return replaced;
}

stock GetRandomReplacer(char[] replacer, int length) {
    int k;
    int nb_replacers;
    
    nb_replacers = GetArraySize(g_aCensorReplacers);

    if (nb_replacers == 0) {
        strcopy(replacer, length, "****");
        return;
    }
    
    k = GetRandomInt(0, nb_replacers-1);
    GetArrayString(g_aCensorReplacers, k, replacer, length);
}

stock bool IsHideable(char[] str) {
    int i;
    char expression[MAXLENGTH_MESSAGE];
    
    for (i = 0; i < GetArraySize(g_aHide); i++){
        GetArrayString(g_aHide, i, expression, sizeof(expression));
        if (0 < SimpleRegexMatch(str, expression, PCRE_CASELESS|PCRE_UTF8)) {
            return true;
        }
    }

    return false;
}

stock int CheckSilence(char[] str) {
    int i, value, size;
    Handle regex;
    char expression[MAXLENGTH_MESSAGE];
    char subString[MAXLENGTH_MESSAGE];
    int minutes = -1;

    size = GetArraySize(g_aGag);
    for (i = 0; i < size; i++) {
        GetArrayString(g_aGag, i, expression, sizeof(expression));
        regex = CompileRegex(expression, PCRE_CASELESS|PCRE_UTF8);
        if (0 < MatchRegex(regex, str)) {
            GetRegexSubString(regex, 0, subString, sizeof(subString));
            value = GetArrayCell(g_aGagTime, i);
            if (value > minutes && minutes != 0) {
                minutes = value;
            }
        }
        CloseHandle(regex);
    }

    return minutes;
}

stock int CheckBan(char[] str) {
    int i, value;
    Handle regex;
    char expression[MAXLENGTH_MESSAGE];
    char subString[MAXLENGTH_MESSAGE];
    int minutes = -1;
    int size;

    size = GetArraySize(g_aBan);
    for (i = 0; i < size; i++) {
        GetArrayString(g_aBan, i, expression, sizeof(expression));
        regex = CompileRegex(expression, PCRE_CASELESS|PCRE_UTF8);
        if (0 < MatchRegex(regex, str)) {
            GetRegexSubString(regex, 0, subString, sizeof(subString));
            value = GetArrayCell(g_aBanTime, i);
            if (value > minutes && minutes != 0) {
                minutes = value;
            }
        }
        CloseHandle(regex);
    }

    return minutes;
}

stock HideWhitelistedExpressions(char[] str, int length, ArrayList whitelisted) {
    int i, k, count = 0;
    char expression[MAXLENGTH_MESSAGE];
    char subString[MAXLENGTH_MESSAGE];
    Handle regex = null;
    int substr_nb = -1;
    char replacer[MAXLENGTH_MESSAGE];

    
    for (i = 0; i < GetArraySize(g_aWhitelist); i++) {
        GetArrayString(g_aWhitelist, i, expression, sizeof(expression));
        regex = CompileRegex(expression, PCRE_CASELESS|PCRE_UTF8);
        substr_nb = MatchRegex(regex, str);
        for (k = 0; k < substr_nb; k++) {
            GetRegexSubString(regex, k, subString, sizeof(subString));
            if (FindStringInArray(whitelisted, subString) == -1) {
                PushArrayString(whitelisted, subString);
                Format(replacer, sizeof(replacer), "{%d}", count);
                ReplaceString(str, length, subString, replacer);
                count++;
            }
        }
        CloseHandle(regex);
    }
}

stock ShowWhitelistedExpressions(char[] str, int length, ArrayList whitelisted) {
    int k, count = 0;
    char subString[MAXLENGTH_MESSAGE];
    char replacer[MAXLENGTH_MESSAGE];
    int nb_replaced;


    nb_replaced = GetArraySize(whitelisted);
    
    for (k = 0; k < nb_replaced; k++) {
        GetArrayString(whitelisted, k, subString, sizeof(subString));
        Format(replacer, sizeof(replacer), "{%d}", count);
        ReplaceString(str, length, replacer, subString);
    }

}

stock ProtectString(char[] str, int length) {
    Handle regex = null;
    int substr_nb = -1;
    int k;
    char subString[MAXLENGTH_MESSAGE];
    char replacer[MAXLENGTH_MESSAGE];
    StringMap alreadyDone = CreateTrie();

    regex = CompileRegex("[0-9]", PCRE_CASELESS|PCRE_UTF8);
    
    substr_nb = MatchRegex(regex, str);
    
    for (k = 0; k < substr_nb; k++) {
        GetRegexSubString(regex, k, subString, sizeof(subString));
        if (SetTrieValue(alreadyDone, subString, 1, false)) {
            Format(replacer, sizeof(replacer), "#%s", subString);
            ReplaceString(str, length, subString, replacer);
        }
        
    }

    CloseHandle(regex);
    CloseHandle(alreadyDone);
}

stock UnprotectString(char[] str, int length) {
    Handle regex = null;
    int substr_nb = -1;
    int k;
    char subString[MAXLENGTH_MESSAGE];
    char replacer[MAXLENGTH_MESSAGE];
    StringMap alreadyDone = CreateTrie();

    regex = CompileRegex("#[0-9]", PCRE_CASELESS|PCRE_UTF8);
    
    substr_nb = MatchRegex(regex, str);
    
    for (k = 0; k < substr_nb; k++) {
        GetRegexSubString(regex, k, subString, sizeof(subString));
        if (SetTrieValue(alreadyDone, subString, 1, false)) {
            strcopy(replacer, sizeof(replacer), subString);
            ReplaceString(replacer, sizeof(replacer), "#", "");
            ReplaceString(str, length, subString, replacer);
        }
        
    }

    CloseHandle(regex);
    CloseHandle(alreadyDone);
}

public Action:SendFilterMessage(Handle:timer, any:userid) {
    new client = GetClientOfUserId(userid);
    if (client == 0) {
        return Plugin_Stop;
    }
    PrintToChat(client, "\x03[CF] %s", g_HideMessage);
    return Plugin_Stop;
}

public Action:PrintDelayedMessageAll(Handle:timer, Handle pack) {
    char message[MAXCHAR];

    ResetPack(pack);
    ReadPackString(pack, message, sizeof(message));

    PrintToChatAll(message);

    return Plugin_Stop;
}

public Action GagPlayerDelayed(Handle timer, Handle pack) {
    int userid;
    int author;
    int minutes;
    int matchType;
    char message[MAXCHAR];

    ResetPack(pack);
    userid = ReadPackCell(pack);
    minutes = ReadPackCell(pack);
    ReadPackString(pack, message, sizeof(message));
    
    author = GetClientOfUserId(userid);
    if (author == 0) {
        return Plugin_Stop;
    }
    
    GagPlayer(author, minutes, message);
    return Plugin_Stop;
}

public Action BanPlayerDelayed(Handle timer, Handle pack) {
    int userid;
    int author;
    int minutes;
    int matchType;
    char message[MAXCHAR];

    ResetPack(pack);
    userid = ReadPackCell(pack);
    minutes = ReadPackCell(pack);
    ReadPackString(pack, message, sizeof(message));

    author = GetClientOfUserId(userid);
    if (author == 0) {
        return Plugin_Stop;
    }
    
    BanPlayer(author, minutes, message);
    return Plugin_Stop;
}

stock FilterType:GetFilterType(Handle:aWords, Handle:aWordsType, String:word[], String:buffer[], maxlen) {
    new i;
    decl String:insult[256];
    decl String:error[256];
    new RegexError:errcode;
    new Handle:regex = INVALID_HANDLE;
    
    for (i = 0; i < GetArraySize(aWords); i++) {
        GetArrayString(aWords, i, insult, sizeof(insult));
        regex = CompileRegex(insult, PCRE_CASELESS|PCRE_UTF8, error, sizeof(error), errcode);
        if (regex == INVALID_HANDLE)
            return FilterNone;
        if (MatchRegex(regex, word) > 0) {
            GetRegexSubString(regex, 0, buffer, maxlen);
            new FilterType:type = GetArrayCell(aWordsType, i);
            CloseHandle(regex);
            return type;
        }
        CloseHandle(regex);
    }

    return FilterNone;
}

stock GetMatchType (String:str[])
{
    if (SimpleRegexMatch (str, "[0-9][0-9]?[0-9]?\\.[0-9][0-9]?[0-9]?\\.[0-9][0-9]?[0-9]?\\.[0-9][0-9]?[0-9]?") > 0)
        return TYPE_ADVERT;
    if (SimpleRegexMatch (str, "(^| )(\\w|-)+\\.(com|net|fr|co\\.uk|org|eu|info|es|it|de|be)", PCRE_CASELESS) > 0)
        return TYPE_ADVERT;
    
    return TYPE_INSULT;
}

stock BanPlayer(client, minutes, String:msg[], type=TYPE_NONE) {
    decl String:reason[MAXCHAR];
    
    if ((type & TYPE_INSULT) == TYPE_INSULT) {
        Format(reason, sizeof(reason), "CF (Insulte): %s", msg);
    } else if ((type & TYPE_ADVERT) == TYPE_ADVERT) {
        Format(reason, sizeof(reason), "CF (Pub): %s", msg);
    } else if ((type & (TYPE_ADVERT|TYPE_INSULT)) == (TYPE_ADVERT|TYPE_INSULT)) {
        Format(reason, sizeof(reason), "CF (Insulte + Pub): %s", msg);
    } else {
        Format(reason, sizeof(reason), "CF: %s", msg);
    }
    
    if (g_bSBAvailable) {
        SBBanPlayer(0, client, minutes, reason);
    } else {
        BanClient(client, minutes, BANFLAG_AUTO, reason, reason);
    }
}

stock GagPlayer(client, minutes, String:msg[], type=TYPE_NONE) {
    decl String:reason[MAXCHAR];
    
    if ((type & TYPE_INSULT) == TYPE_INSULT) {
        Format(reason, sizeof(reason), "CF (Insulte): %s", msg);
    } else if ((type & TYPE_ADVERT) == TYPE_ADVERT) {
        Format(reason, sizeof(reason), "CF (Pub): %s", msg);
    } else if ((type & (TYPE_ADVERT|TYPE_INSULT)) == (TYPE_ADVERT|TYPE_INSULT)) {
        Format(reason, sizeof(reason), "CF (Insulte + Pub): %s", msg);
    } else {
        Format(reason, sizeof(reason), "CF: %s", msg);
    }
    
    if (g_bSCAvailable) {
        if (minutes == 0) {
            minutes = 9999999;
        }
        
        SourceComms_SetClientGag(client, true, minutes, true, reason);
        SourceComms_SetClientMute(client, true, minutes, true, reason);
        
        return;
    } 
    
    if (g_bTGAvailable) {
        TGSilencePlayer(0, client, minutes, reason, false);
        return;
    }
    
    /* If TimedGags and SourceComms are unavailable */
    
    BaseComm_SetClientGag(client, true);
    BaseComm_SetClientMute(client, true);
    PrintToChat(client, "[CF] You are now silenced !");
}

stock bool:ClientProtected (client) {
    new AdminId:admin;
    
    if (!client || !IsClientInGame(client))
        return true;
    
    admin = GetUserAdmin(client);
    
    if (admin == INVALID_ADMIN_ID)
        return false;
    
    if (GetAdminFlag(admin, Admin_Generic))
        return true;
    
    return false;
}

stock ConvertToAscii (String:str[]) {
    new i = 1, j = 1;
    
    if (str[0] == '\0')
        return;
    
    while (str[i] != '\0') {
        if (str[i-1] & 0xC3 == 0xC3) {
            if ((str[i] >= 128 && str[i] <= 133) || (str[i] >= 160 && str[i] <= 165)) {
                str[j-1] = 'a' ;
                j--;
            } else if ((str[i] >= 136 && str[i] <= 139) || (str[i] >= 168 && str[i] <= 171)) {
                str[j-1] = 'e' ;
                j--;
            } else if ((str[i] >= 140 && str[i] <= 143) || (str[i] >= 172 && str[i] <= 175)) {
                str[j-1] = 'i' ;
                j--;
            } else if ((str[i] >= 146 && str[i] <= 150) || (str[i] >= 178 && str[i] <= 182)) {
                str[j-1] = 'o' ;
                j--;
            } else if ((str[i] >= 153 && str[i] <= 156) || (str[i] >= 185 && str[i] <= 188)) {
                str[j-1] = 'u' ;
                j--;
            } else if (str[i] == 135 || str[i] == 167) {
                str[j-1] = 'c' ;
                j--;
            } else {
                str[j] = str[i];
            }
        } else {
            str[j] = str[i];
        }
        
        i++;
        j++;
    }
    
    str[j] = '\0';
}

stock CS_SetClientName(client, const String:name[], bool:silent=false) {
    decl String:oldname[MAX_NAME_LENGTH];
    GetClientName(client, oldname, sizeof(oldname));

    SetClientInfo(client, "name", name);
    SetEntPropString(client, Prop_Data, "m_szNetname", name);

    new Handle:event = CreateEvent("player_changename");

    if (event != INVALID_HANDLE) {
        SetEventInt(event, "userid", GetClientUserId(client));
        SetEventString(event, "oldname", oldname);
        SetEventString(event, "newname", name);
        FireEvent(event);
    }

    if (silent)
        return;
    
    new Handle:msg = StartMessageAll("SayText2");

    if (msg != INVALID_HANDLE) {
        BfWriteByte(msg, client);
        BfWriteByte(msg, true);
        BfWriteString(msg, "Cstrike_Name_Change");
        BfWriteString(msg, oldname);
        BfWriteString(msg, name);
        EndMessage();
    }
}

public Action:Command_Reload(client, args) {
    ResetSettings();
    return Plugin_Handled;
}

stock ResetSettings() {
    decl String:buffer[2048];
    decl String:replacers[32][MAXLENGTH_REPLACER];
    int nb_splits, k;
    
    ClearArray(g_aGag);
    ClearArray(g_aGagTime);
    ClearArray(g_aBan);
    ClearArray(g_aBanTime);
    ClearArray(g_aWhitelist);
    ClearArray(g_aCensor);
    ClearArray(g_aCensorReplacers);
    ClearArray(g_aHide);
    ClearArray(g_aNameFilter);
    ClearArray(g_aNameFilterType);
    ClearArray(g_aClanFilter);
    ClearArray(g_aClanFilterType);
    
    g_bProtectAdmins = GetConVarBool(g_Cvar_ProtectAdmins);
    g_bAddTime = GetConVarBool(g_Cvar_AddTime);
    GetConVarString(g_Cvar_HideMessage, g_HideMessage, sizeof(g_HideMessage));
    GetConVarString(g_Cvar_TagName, g_TagName, sizeof(g_TagName));
    GetConVarString(g_Cvar_NameKickReason, g_NameKickReason, sizeof(g_NameKickReason));
    GetConVarString(g_Cvar_ClanKickReason, g_ClanKickReason, sizeof(g_ClanKickReason));
    GetConVarString(g_Cvar_NameBanReason, g_NameBanReason, sizeof(g_NameBanReason));
    GetConVarString(g_Cvar_ClanBanReason, g_ClanBanReason, sizeof(g_ClanBanReason));
    g_NameBanTime = GetConVarInt(g_Cvar_NameBanTime);
    g_ClanBanTime = GetConVarInt(g_Cvar_ClanBanTime);

    GetConVarString(g_Cvar_CensorReplacers, buffer, sizeof(buffer));

    nb_splits = ExplodeString(buffer, ";", replacers, 32, MAXLENGTH_REPLACER);
    for (k = 0; k < nb_splits; k++) {
        PushArrayString(g_aCensorReplacers, replacers[k]);
    }
    
    QueryDatabase ();
}

// --- Database Code --- //


public GotDatabase(Handle:owner, Handle:hndl, const String:error[], any:data) {
    if (hndl == INVALID_HANDLE) {
        LogToFile(log, "Database failure: %s", error);
    } else {
        g_bConnected = true;
        hDatabase = hndl;
        SQL_FastQuery (hDatabase, "SET NAMES \"UTF8\"");
    }
}

stock QueryDatabase() {
    decl String:query[512];
    
    Format(query, sizeof(query), "SELECT word, type, minutes FROM cf_chat");
    SQL_TQuery(hDatabase, SelectWordCallback, query);
    
    Format(query, sizeof(query), "SELECT name, type FROM cf_name");
    SQL_TQuery(hDatabase, SelectNameCallback, query);
    
    Format(query, sizeof(query), "SELECT tag, type FROM cf_tag");
    SQL_TQuery(hDatabase, SelectTagCallback, query);
    
    PrintToServer("[SM] Loading filtered words from the database");
}

public SelectWordCallback(Handle:owner, Handle:hndl, const String:error[], any:data) {
    new minutes;
    decl String:word[512];
    decl String:type[32];
    
    if (hndl == INVALID_HANDLE || error[0]) {
        LogToFile(log, "Select Word Query Failed: %s", error);
        return;
    }
    
    while (SQL_FetchRow (hndl)) {
        SQL_FetchString(hndl, 0, word, sizeof(word));
        SQL_FetchString(hndl, 1, type, sizeof(type));
        minutes = SQL_FetchInt (hndl, 2);
        
        if (!strcmp(type, "whitelist", false)) {
            PushArrayString(g_aWhitelist, word);
        } else if (!strcmp(type, "censor", false)) {
            PushArrayString(g_aCensor, word);
        } else if (!strcmp(type, "hide", false)) {
            PushArrayString(g_aHide, word);
        } else if (!strcmp(type, "gag", false)) {
            PushArrayString(g_aGag, word);
            PushArrayCell(g_aGagTime, minutes);
        } else if (!strcmp(type, "ban", false)) {
            PushArrayString(g_aBan, word);
            PushArrayCell(g_aBanTime, minutes);
        } else {
            LogToFile(log, "Type %s is invalid !", type);
        }
    }
    
    PrintToServer("[SM] Filtered chat words loaded");
}

public SelectNameCallback(Handle:owner, Handle:hndl, const String:error[], any:data) {
    decl String:name[512];
    decl String:type[32];
    
    if (hndl == INVALID_HANDLE || error[0]) {
        LogToFile(log, "Select Name Query Failed: %s", error);
        return;
    }
    
    while (SQL_FetchRow(hndl)) {
        SQL_FetchString(hndl, 0, name, sizeof(name));
        SQL_FetchString(hndl, 1, type, sizeof(type));
        
        if (!strcmp(type, "kick", false)) {
            PushArrayString(g_aNameFilter, name);
            PushArrayCell(g_aNameFilterType, FilterKick);
        } else if (!strcmp(type, "ban", false)) {
            PushArrayString(g_aNameFilter, name);
            PushArrayCell(g_aNameFilterType, FilterBan);
        } else if (!strcmp(type, "rename", false)){
            PushArrayString(g_aNameFilter, name);
            PushArrayCell(g_aNameFilterType, FilterRename);
        } else {
            LogToFile(log, "Type %s is invalid !", type);
        }
    }
    
    PrintToServer("[SM] Filtered names loaded");
}

public SelectTagCallback(Handle:owner, Handle:hndl, const String:error[], any:data) {
    decl String:tag[512];
    decl String:type[32];
    
    if (hndl == INVALID_HANDLE || error[0]) {
        LogToFile(log, "Select Tag Query Failed: %s", error);
        return;
    }
    
    while (SQL_FetchRow(hndl)) {
        SQL_FetchString(hndl, 0, tag, sizeof(tag));
        SQL_FetchString(hndl, 1, type, sizeof(type));
        
        if (!strcmp (type, "hide", false)) {
            PushArrayString(g_aClanFilter, tag);
            PushArrayCell(g_aClanFilterType, FilterHide);
        } else if (!strcmp(type, "kick", false)) {
            PushArrayString(g_aClanFilter, tag);
            PushArrayCell(g_aClanFilterType, FilterKick);
        } else if (!strcmp(type, "ban", false)) {
            PushArrayString(g_aClanFilter, tag);
            PushArrayCell(g_aClanFilterType, FilterBan);
        } else {
            LogToFile(log, "Type %s is invalid !", type);
        }
    }
    
    PrintToServer("[SM] Filtered tags loaded");
}
