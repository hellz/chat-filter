#if defined _timedgags_included
  #endinput
#endif
#define _timedgags_included

public SharedPlugin:__pl_timedgags = 
{
	name = "TimedGags",
	file = "timedgags.smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public __pl_timedgags_SetNTVOptional()
{
	MarkNativeAsOptional("TGGagPlayer");
	MarkNativeAsOptional("TGMutePlayer");
	MarkNativeAsOptional("TGSilencePlayer");
}
#endif


/*********************************************************
 * Gag Player from server
 *
 * @param client		The client index of the admin who is gagging the client
 * @param target		The client index of the player to gag
 * @param time			The time to gag the player for (in minutes, 0 = permanent)
 * @param reason		The reason to gag the player from the server
 * @param showReason	Show the reason if true in gag message
 * @noreturn		
 *********************************************************/
native TGGagPlayer(client, target, time, String:reason[], bool:showReason=true);

/*********************************************************
 * Mute Player from server
 *
 * @param client		The client index of the admin who is muting the client
 * @param target		The client index of the player to mute
 * @param time			The time to mute the player for (in minutes, 0 = permanent)
 * @param reason		The reason to mute the player from the server
 * @param showReason	Show the reason if true in mute message
 * @noreturn		
 *********************************************************/
native TGMutePlayer (client, target, time, String:reason[], bool:showReason=true);

/*********************************************************
 * Silence Player from server
 *
 * @param client		The client index of the admin who is silencing the client
 * @param target		The client index of the player to silence
 * @param time			The time to silence the player for (in minutes, 0 = permanent)
 * @param reason		The reason to silence the player from the server
 * @param showReason	Show the reason if true in silence message
 * @noreturn		
 *********************************************************/
native TGSilencePlayer (client, target, time, String:reason[], bool:showReason=true);